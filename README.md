# App Infrastructure with Terraform


In this Terraform example we will discuss how to provision an infrastructure for mobile apps application to deploy backend-api in GKE cluster, Use cloud-sql for postgresql DB and set identity platform for firebase authentication.


## Infrastructure
In the infrastructure folder we have all terraform staff to provision the infrastructure in GCP.


I will describe different files or folder purposes below to have an idea how actually this code will work.


In the **module** directory we will find 3 separated modules with dynamic use cases.


- **db:** DB module has resource of postgresql cloud sql instance and database provisioning features.
- **gke:** GKE module has resources of GKE cluster and nodepool provisioning features.
- **identity-platform:** the identity-platform module has resources of GCP identity platform provisioning features.


All the modules above are using dynamic variables options to re-use in different environment settings.


- **provider.tf** : This is used for configuring cloud providers, in our case its GCP.
- **terraform-backend.tf** : This file is used to configure remote backend setup to store terraform state in a secure storage bucket and make the state available for any user.
- **main.tf** : This file has all references of modules/resources which will be provisioned. Actually this is the main file where we put everything to provision.
- **variables.tf** : This file contains all required variables declaration to use terraform apply or plan.
- **variables.auto.tfvars** : In this file we put all variables values and it will be used automatically terraform **plan or apply** command to provision respective infrastructure.


### How can use terraform to provision infra
All things we need we have available in the example. you just need to change the **variables.auto.tfvars** file values with your specific values. Then you are ready to go.


Now we can provision infra via below command (if terraform is installed already in your PC)


```
cd infrastructure
terraform init # initialize
terraform plan # check what type of resource will be created
terraform apply # Now provisioning the resource in the GCP
```
Now you will access/find all resources in your GCP project.


If you want destroy all just run below command


```
terraform destroy
```
Hope you have a good hands on how to provision infra for mobile apps.
