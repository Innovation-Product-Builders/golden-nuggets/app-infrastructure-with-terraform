#### Project Access variables
variable "project_id" {
  description = "The ID of the project in which to create the GKE cluster."
}
variable "credentials" {
  description = "The ID of the project in which to create the GKE cluster."
}
variable "region" {
  description = "The region of the project resources."
}

##### GKE cluster and nodepool
variable "clusterName" {
  description = "The name of the GKE cluster."
}

variable "zone" {
  description = "The zone in which to create the GKE cluster."
}

variable "initial_node" {
  type = number
  description = "The number of initial node cluster."
}

variable "node_pools" {
  description = "A list of node pool configurations for the GKE cluster."
  type = list(object({
    name         = string
    machine_type = string
    node_count   = number
    disk_size_gb = number
  }))
}

### GCP cloud SQL Instance and DB 
variable "database_instance_name" {
  description = "The name of the cloud SQL instance."
}

variable "database_instance_location" {
  description = "The location in where to create the cloud SQL instance."
}

variable "database_version" {
  description = "Which POSTGRESQL version will be used in the Instance."
}

variable "database_instance_machine_type" {
    description = "The machine type to use"
}

variable "database_name" {
    description = "The DB name which will be created in the instance"
}

#### Identity Platform variables

variable "test_phone_number_map" {
   type = map(string)
}