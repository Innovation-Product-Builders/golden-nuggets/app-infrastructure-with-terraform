
### Project Access variables
credentials = "./serviceaccount_credentials.json" ## Location of the service account credentials
project_id = "test-platform-lab" ## your GCP project-id
region= "us-central1" ## Region of project resource

### GKE cluster and Nodepool  variables
clusterName       = "test-cluster-lab" ## Cluster name 
zone       = "us-central1-c" ## cluster zone as we are creating zonal cluster here 
initial_node = 1 ## Number of node in initial/default node-pool

## list of customize nodepool 
node_pools = [
  {
    name         = "lab-pool"
    machine_type = "e2-standard-2"
    node_count   = 1
    disk_size_gb = 40
  },
]

### DB instance and DB settings variables
database_instance_name = "test-postgresql-instance" ## CloudSQL instance name
database_instance_location = "us-central1" ## region of the instance
database_version = "POSTGRES_15" ## Instance type
database_instance_machine_type = "db-f1-micro" ## Instance machine type
database_name = "test-db" ## Custom Database name

### Identity Platform related variables
test_phone_number_map = {
        "+4915231626000" = "123456" # list of test phone numbers
      }
