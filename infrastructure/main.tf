module "gke_cluster_and_nodepool" {
   source = "./module/gke"
   clusterName = var.clusterName
   zone = var.zone
   node_pools = var.node_pools
   initial_node = var.initial_node
}

module "gcp_sql-instance_and_db"{
    source = "./module/db"
    database_instance_name = var.database_instance_name
    database_instance_location = var.database_instance_location
    database_version = var.database_version
    database_instance_machine_type = var.database_instance_machine_type
    database_name = var.database_name
}

module "gcp_identity_platform" {
    source = "./module/identy-platform"
    test_phone_number_map = var.test_phone_number_map
}