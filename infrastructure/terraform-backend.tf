terraform {
  backend "gcs" {
    bucket      = "test-terraform-lab"
    prefix      = "terraform-lab"
    credentials = "./serviceaccount_credentials.json"
  }
}
