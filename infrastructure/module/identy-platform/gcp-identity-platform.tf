
resource "google_identity_platform_project_default_config" "default" {
  sign_in {
    email {
      enabled = true
      password_required = true
    }
    phone_number {
      enabled = true
      test_phone_numbers = var.test_phone_number_map
    }
  }
}