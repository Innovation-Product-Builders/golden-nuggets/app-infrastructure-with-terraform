resource "google_sql_database_instance" "database_instance" {
  name             = var.database_instance_name
  region           = var.database_instance_location
  database_version = var.database_version
  settings {
    tier = var.database_instance_machine_type
  }

  deletion_protection  = "true"
}

resource "google_sql_database" "create_database" {
  name     = var.database_name
  instance = google_sql_database_instance.database_instance.name
  deletion_policy = "ABANDON"
}
