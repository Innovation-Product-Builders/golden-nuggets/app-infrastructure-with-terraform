
variable "database_instance_name" {
  description = "The name of the cloud SQL instance."
}

variable "database_instance_location" {
  description = "The location in where to create the cloud SQL instance."
}

variable "database_version" {
  description = "Which POSTGRESQL version will be used in the Instance."
}

variable "database_instance_machine_type" {
    description = "The machine type to use"
}

variable "database_name" {
    description = "The DB name which will be created in the instance"
}