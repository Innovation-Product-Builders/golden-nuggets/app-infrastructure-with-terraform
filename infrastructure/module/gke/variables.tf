
variable "clusterName" {
  description = "The name of the GKE cluster."
}

variable "zone" {
  description = "The zone in which to create the GKE cluster."
}

variable "initial_node" {
  type = number
  description = "The number of initial node cluster."
}

variable "node_pools" {
  description = "A list of node pool configurations for the GKE cluster."
  type = list(object({
    name         = string
    machine_type = string
    node_count   = number
    disk_size_gb = number
  }))
}
