
resource "google_container_cluster" "primary" {
  name                     = var.clusterName
  location                 = var.zone
  initial_node_count       = var.initial_node
  remove_default_node_pool = true
}

resource "google_container_node_pool" "my_node_pool" {

  for_each   = { for node in var.node_pools : node.name => node }
  name       = each.value.name
  location   = var.zone
  cluster    = google_container_cluster.primary.name
  node_count = each.value.node_count

  node_config {
    machine_type = each.value.machine_type
    disk_size_gb = each.value.disk_size_gb
    preemptible  = false
  }
}